<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js" integrity="sha512-/bOVV1DV1AQXcypckRwsR9ThoCj7FqTV2/0Bm79bL3YSyLkVideFLE3MIZkq1u5t28ke1c0n31WYCOrO01dsUg==" crossorigin="anonymous"></script>
<script>


// axios instance for form data submission
var shopifyAPI = axios.create({
    baseURL: 'https://api-manager.hulkcode.com/api/shopify/common',
    headers: {
      'Content-Type' : 'application/json',
      'X-Shop': 'dermaclara.myshopify.com'
    },
  });
var rechargeAPI = axios.create({
    baseURL: 'https://api-manager.hulkcode.com/api/recharge/common',
    headers: {
      'Content-Type' : 'application/json',
      'X-Shop': 'dermaclara.myshopify.com'
    },
  });
var Upsell= {
  orderId : '{{id}}',
  upsellPrdjson: null,
  line_itemsJSON: [],
  init:function(){
    this.getOrderDetails();
  },
getOrderDetails:function(){
   var config = {
            "method" : "GET",
	     "url" : "/orders/"+Upsell.orderId+".json?status=any&fields=line_items"
          };

     shopifyAPI.post('https://api-manager.hulkcode.com/api/shopify/common',config)
          .then((response) => {
var line_items =response.data.order.line_items;
this.getUpsellProducts(line_items);

});
 },
getUpsellProducts : function(line_items){
var $this = this;
var data = { "line_items": line_items};
     $.ajax({
            type: "POST",
            url: "https://code.hulkapps.com/dermaclara/apigetupsellsubproducts.php",
            data: data,
            dataType: "json",
            success: function (response, status, xhr) {
                 //console.log(response);
                $this.displayUpsell(response.data);
            },
            error: function (xhr, status, error) {
               console.log(error);
            }
        });


 },
 displayUpsell: function(products){
 var productshtml = '';
  var upsellprds= [];
  for (var key in products){

   $(products[key]).each(function(i,subprd){
    upsellprds.push(subprd.sub_product_id);
   });
  }
if(upsellprds.length){
var upsellprds = upsellprds.filter(function(elem, index, self) {
      return index === self.indexOf(elem);
  });
var config = {
            "method" : "GET",
	     "url" : "/products.json?ids="+upsellprds.join(',')+"&fields=id,variants,images,title,handle"
          };

     shopifyAPI.post('https://api-manager.hulkcode.com/api/shopify/common',config)
          .then((response) => {
     $(response.data.products).each(function(i,prd){
productshtml += '<div class="col-4 col-item  text-center item">'+
            '<div class="product-img-border">'+
              '<div class="prod-img-wrap">'+
                '<a href="https://www.dermaclara.com/products/'+prd.handle+'">'+
                  '<img class="fade-in lazyloaded" src="'+prd.images[0].src+'">'+
                '</a>'+
              '</div>'+
              '<div class="product-detail">'+
                '<a class="title-product" href="https://www.dermaclara.com/products/'+prd.handle+'">'+prd.title+'</a>'+
                '<span class="price">$'+prd.variants[0].price+'</span>'+
              '</div>'+
            '</div>'+
'<p class="text-center buy-button"><a href="#" data-price='+prd.variants[0].price+' data-varid='+prd.variants[0].id+' id="add" onclick="Upsell.updateitems(event);" class="text-center btn" >ADD</a></p>'+
        '</div>';
})
$('#productCarousel').append(productshtml);
$('.product-detail').matchHeight();
   $("#productCarousel").owlCarousel({
    autoplay: false,
    lazyLoad: true,
    loop: false,
    items:2,
    margin: 20,
    dots: false,
    thumbs: false,
    responsiveClass: true,
    autoHeight: true,
    nav: true,
  	responsive: {
    0: {
      items: 1,
     nav: true

    },
480: {
      items: 2,
     nav: true

    },

    600: {
      items: 3,
nav: true
    },

    1024: {
      items: 3,
nav: true
    },

    1366: {
      items: 3,
nav: true
    }
  }
}); 
});

   var html ='<div class="upsellProducts">'+
'<h3>Add a Product to your existing order</h3>'+
'<h3>(You can only choose one)</h3>'+
'<div  id="productCarousel" class="row related-product-youth  owl-carousel owl-theme">'+
'</div>'+
    '<p class="text-center"><a href="#" style="display:none;" id="AddToCart" class="addtocart text-center btn btn-lg btn-blue" onclick="Upsell.updateOrder(event);" >ADD TO ORDER</a></p>'+
'</div>'+'<div class="thank-you container" style="display:none;">'+
                      '<div class="row">'+
                        '<div class="thanks-buying">'+
                            '<div class="confirm-submit">'+
                                '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"'+
                                    'viewBox="-235.7 368.1 60 60" style="enable-background:new -235.7 368.1 60 60;" xml:space="preserve">'+
                                '<polygon points="-177.4,376.5 -217.3,416.5 -234.1,398.5 -235.7,400.1 -217.3,419.7 -216.9,419.2 -216.9,419.3'+ 
                                    '-175.7,378.2 "/>'+
                                '</svg>'+
                            '</div>'+
                            '<h5>Thank you for buying from us</h5>'+
                            '<p class="text-gray-600">Your product will be delivered in your next shipment</p>'+
                        '</div>'+
                         '</div>'+
                    '</div>';
$('#checkout').append(html);
}

 },
updateitems: function(e){
 e.preventDefault();
 var newLineItemJson = null; 
 var id = null;
 var price = null;
 if($(e.currentTarget).attr('id') == 'add'){
   id = $(e.currentTarget).data('varid');
   price = $(e.currentTarget).data('price')
   newLineItemJson= {
                      "variant_id" : id,
                      "price": price
                    }
  Upsell.line_itemsJSON.push(newLineItemJson);
  $(e.currentTarget).attr('id', 'remove')
  $(e.currentTarget).text('REMOVE');
 } else if($(e.currentTarget).attr('id') == 'remove'){
  id = $(e.currentTarget).data('varid');
  Upsell.line_itemsJSON = Upsell.line_itemsJSON.filter(x => x.variant_id !== id);
  $(e.currentTarget).attr('id', 'add')
  $(e.currentTarget).text('ADD');
}

if(Upsell.line_itemsJSON.length == 1){
  $('#AddToCart').show();
} else{
 $('#AddToCart').hide();
 }
},
updateOrder: function(e){
$('.addtocart').text('Please Wait ...');
  e.preventDefault();
            var date = new Date();
            var d = date.getDate();
            var m =  date.getMonth()+1;
            var y = date.getFullYear();
            var todayDate = y+'-'+m+'-'+d;
            var charge_id = window.cart_json.charge_id;
            var config = {
            "method" : "GET",
	     "url" : "/charges/"+charge_id
          };

     rechargeAPI.post('https://api-manager.hulkcode.com/api/recharge/common',config)
          .then((response) => {
                    addOneTime(response.data.charge.address_id);
});
function addOneTime(addressId){
                var onetime = { 
                    "url":"/onetimes/address/"+addressId,
                    "method":"POST",
                    data: {
                    "next_charge_scheduled_at": todayDate,
                    "price": (Upsell.line_itemsJSON)[0].price,
                    "quantity": 1,
                    "shopify_variant_id": (Upsell.line_itemsJSON)[0].variant_id,
                      "properties": {
                        "upsell": "onetime"
                      },
                    },
                };
rechargeAPI.post('https://api-manager.hulkcode.com/api/recharge/common',onetime)
          .then((response) => {
      //console.log(response)
$('.upsellProducts').hide();
$('.thank-you').show();
});

}
}
}
$(document).ready(function(){
   Upsell.init();
});



</script>

<style>
.related-product-youth .col-item .product-detail a.btn {
	 margin: 0 auto 0;
	 font-size: 16px !important;
	 width: auto;
	 height: auto;
	 line-height: 33px;
	 background: #1e1e1e;
	 font-weight: 600 !important;
}
 #productCarousel {
	 margin-left: 0;
	 margin-right: 0;
}
 #productCarousel .owl-nav {
	 position: absolute;
	 top: 28%;
	 -webkit-transform: translate(0%, -50%);
	 -moz-transform: translate(0%, -50%);
	 -o-transform: translate(0%, -50%);
	 -ms-transform: translate(0%, -50%);
	 transform: translate(0%, -50%);
	 left: 0;
	 width: 100%;
}
 #productCarousel .owl-nav > button {
	 background-position: center center !important;
	 background: transparent;
	 text-indent: -9999px;
	 padding: 0;
	 margin: 0;
	 position: absolute;
	 background-repeat: no-repeat;
	 background-size: 16px auto;
	 width: 18px;
	 height: 37px;
	 opacity: 1;
	 top: 50px;
	 cursor: pointer;
         outline: none;
}
 #productCarousel .owl-nav .owl-prev {
	 background-image: url('https://cdn.shopify.com/s/files/1/1009/6506/files/arrow-icon-left.png?v=1598431465');
	 left: -15px;
}
 #productCarousel .owl-nav .owl-next {
	 background-image: url('https://cdn.shopify.com/s/files/1/1009/6506/files/arrow-icon-right.png?v=1598431452');
	 right: -15px;
}
 .related-product-youth .product-img-border {
	 padding: 0 10px;
}
 .related-product-youth .button-wrap {
	 display: flex;
	 align-items: center;
	 justify-content: space-between;
	 margin-top: 10px;
}
 .related-product-youth .button-wrap p {
	 margin: 0;
}
 .related-product-youth .button-wrap select {
	 width: auto;
}
 .related-product-youth a {
	 display: block;
         font-size: 15px;
}
.related-product-youth .price{
    font-size: 16px;
    margin-top: 10px;
    display: block;
}
 .related-product-youth .prod-img-wrap {
	 height: 200px;
}
 .related-product-youth .prod-img-wrap a {
	 height: 100%;
	 display: flex;
	 align-items: center;
	 justify-content: center;
	 width: 100%;
}
 .related-product-youth .prod-img-wrap a img {
	 max-width: 100%;
	 max-height: 100%;
	 width: auto;
	 height: auto !important;
}
.wrap{
  box-sizing:unset;
}

.buy-button a{
  max-width: 80%;
  margin: auto;
 padding: 10px;
margin-top: 10px;
font-size: 18px;
}
button.owl-prev.disabled , button.owl-next.disabled{
 display:none;
}

.addtocart {
  margin-top: 30px;
width:200px;
}
.upsellProducts{
  margin-bottom:30px;
}
.thanks-buying {
  text-align: center;
  max-width: 540px;
}

.thanks-buying .confirm-buying {
  border: 2px solid #88C03A;
  width: 84px;
  height: 84px;
  border-radius: 50%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin: 0 auto 30px;
}
.thanks-buying .confirm-submit {
  border: 2px solid black;
  width: 84px;
  height: 84px;
  border-radius: 50%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin: 0 auto 30px;
}

.thanks-buying .confirm-buying svg {
  fill: #88C03A;
  width: 70%;
  height: 70%;
}
.thanks-buying .confirm-submit svg {
  fill: black;
  width: 70%;
  height: 70%;
}

.thanks-buying h5 {
  font-size: 20px;
  text-transform: initial;
  font-weight: 600;
  margin-bottom: 8px;
}

.thanks-buying p {
  color: #949A9B;
  font-size: 17px;
  margin-bottom: 18px;
}

.thanks-buying .tnk-link-border {
  font-weight: 700;
  font-size: 0.9375rem;
  text-transform: uppercase;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
  text-decoration: none !important;
  display: inline-block;
  border-bottom: 1px solid !important;
  cursor: pointer;
  color: #303536 !important;
}
h3{
  text-align:center;
}
.thanks-buying .tnk-link-border:hover {
  color: #F96361 !important;
}

@media only screen and (max-width:480px){
    #productCarousel .owl-nav .owl-prev {
	 left: 0px;
}
 #productCarousel .owl-nav .owl-next {
	 right: 0px;
}
 
}
 

</style>