
$(document).ready(function(){
  
  $('[data-minus]').click(function(e){
    e.preventDefault();
    if($('[data-count]').val() > 1){
    	$('[data-count]').val(parseInt($('[data-count]').val()) - 1);
    } 
  
  }); // deccrement qty
  $('[data-plus]').click(function(e){
    e.preventDefault();
    $('[data-count]').val(parseInt($('[data-count]').val()) + 1);

  }); // increment qty
  $('[data-addtocart]').click(function(e){
   	e.preventDefault();

       $('.addtocartForm').submit();
  }); // add to cart
  $("#carousel").owlCarousel({
    autoplay: false,
    lazyLoad: true,
    loop: true,
    margin: 20,
    dots: false,
    thumbs: false,
    responsiveClass: true,
    autoHeight: true,
    autoplayTimeout: 7000,
    smartSpeed: 800,
    nav: true,
  	responsive: {
    0: {
      items: 1
     

    },

    600: {
      items: 1
    },

    1024: {
      items: 1
    },

    1366: {
      items: 1
    }
  }
}); // video carousel


  $("#carousel1").owlCarousel({
  autoplay: false,
  lazyLoad: true,
  loop: true,
  margin: 20,
  dots: true,
  thumbs: false,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: false,

  responsive: {
    0: {
      items: 1,
      nav:false

    },

    600: {
      items: 2
    },

    1024: {
      items: 2
    },

    1366: {
      items: 3,
      dots: true
    }
  }
}); // review carousel

  $("#owl-carousel2").owlCarousel({
    loop: true,
    items: 1,
    nav:true,
    slideSpeed: 2000,
    autoplay: true,
    thumbs: true,
    thumbImage: true,
    thumbContainerClass: "owl-thumbs",
    thumbItemClass: "owl-thumb-item"
  }); // Product image carousel
	// Select all links with hashes
  $('.nav-link, .btn-get').not('[href="#"]').not('[href="#0"]').click(function(event) {
          // On-page links
          if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
            && 
            location.hostname == this.hostname
          ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
              // Only prevent default if animation is actually gonna happen
              event.preventDefault();
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                  return false;
                } else {
                  $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                  $target.focus(); // Set focus again
                };
              });
            }
          }
  		});

});




